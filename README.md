
@TODO

* Analytics with geolocation
* Sync streaming with heartbeat
* Watch for #spacefolding on social channels and reply with this link


### Geolocation shims

* https://gist.github.com/paulirish/366184
* http://afarkas.github.io/webshim/demos/index.html#Geolocation

var x = document.getElementById("demo");
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        x.innerHTML = "Geolocation is not supported by this browser.";
    }
}
function showPosition(position) {
    x.innerHTML = "Latitude: " + position.coords.latitude + 
    "<br>Longitude: " + position.coords.longitude; 
}
